---
  title: What is DevSecOps?
  description: Effectively turn your DevOps methodology into a DevSecOps methodology
  components:
    - name: topics-header
      data:
        title: DevSecOps
        read_time: 5 min read
        block:
            - metadata:
                id_tag: what-is-devsecops
              text: |
                  Can your existing DevSecOps and application security keep pace with modern development methods? Learn how next-generation software requires a new approach to app sec.

    - name: topics-copy-block
      data:
        header: What is DevSecOps?
        column_size: 10
        blocks:
          - text: |
              Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by embracing security earlier within the DevOps lifecycle.
            image:
              image_url: /nuxt-images/topics/cost-savings-to-shift-left.png
              alt: cost savings
          - text: DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, improving collaboration and breaking down silos. By embedding security into the SDLC, you can consistently secure fast- moving and iterative processes - improving efficiency without sacrificing quality.
          - text: DevSecOps weaves security practices into every stage of software development right through deployment with the use of tools and methods to protect and monitor live applications. New attack surfaces such as containers and orchestrators must be monitored and protected alongside the application itself.
    - name: topics-copy-block
      data:
        header: What is application security?
        column_size: 10
        blocks:
          - text: Application security has been [defined by TechTarget](https://searchsoftwarequality.techtarget.com/definition/application-security) as the use of software, hardware, and procedural methods to protect applications from external threats. Modern approaches include shifting left to find and fix vulnerabilities earlier, while also shifting right to protect your applications and their infrastructure-as-code in production. Securing the Software Development Life Cycle (SDLC) itself is often a requirement as well.
          - text: This approach of building security into your development and operational processes effectively turns your DevOps methodology into a DevSecOps methodology. An end-to-end DevOps platform can best enable this approach.
    - name: topics-copy-block
      data:
        header: DevSecOps fundamentals
        column_size: 10
        blocks:
            - text: If you’ve read the book that was the genesis for the DevOps movement, The Phoenix Project, you understand the importance of automation, consistency, metrics, and collaboration. For DevSecOps, you are essentially applying these techniques to outfit the software factory while embedding security capabilities along the way rather than in a separate, siloed process. Dev or security can find vulnerabilities, but a developer is usually required to remove such flaws. It makes sense to empower them to find and fix vulnerabilities while they are still working on the code. Scanning alone isn’t enough. It’s about getting the results to the right people, at the right time, with the right context for quick action.
            - text: Fundamental requirements include automation and collaboration, along with policy guardrails and visibility.
    - name: topics-copy-block
      data:
        header: Automation
        column_size: 10
        blocks:
            - text: |
                In our [2020 DevSecOps Survey](/developer-survey/). we found a majority of developers aren’t running SAST, DAST or other security scans regularly, and automation also lags. A majority of security pros reported their DevOps teams are “shifting left,” but test automation continues to be a huge challenge.

                This evolution took place in following four phases:

                ### Collaboration

                A single source of truth that reports vulnerabilities and remediation provides much-needed transparency to both development and security team. It can streamline cycles, eliminate friction, and remove unnecessary translation across tools.

                ### Policy guardrails

                Every enterprise has a different appetite for risk. Your security policies will reflect what is right for you while the regulatory requirements to which you must adhere will also influence the policies you must apply. Hand-in-hand with automation, guardrails can ensure consistent application of your security and compliance policies.

                ### Visibility

                An end-to-end DevSecOps platform can give auditors a clear view into who changed what, where, when, and why from beginning to end of the software lifecyle. Leveraging a single-source-of-truth can also ensure earlier visibility into application risks.
    - name: topics-copy-block
      data:
        header: Getting started
        column_size: 10
        blocks:
            - text: |
                Ready to see how GitLab can help you get started with DevSecOps?

                Our [DevSecOps Solution](/solutions/dev-sec-ops/). page has all of the details, along with a Free Trial offer for our Ultimate tier of capabilities.
    - name: topics-copy-block
      data:
        header: Assess your DevSecOps maturity
        column_size: 10
        blocks:
            - text: We have developed a simple assessment that uses twenty questions to help you determine where you excel and areas for improvement. A helpful guide suggests steps to get you started.
              link:
                url: /quiz/devsecops-html/
                text: Asses yourself
                data_ga_name: Asses yourself
    - name: topics-copy-block
      data:
        header: Benefits of DevSecOps
        column_size: 10
        blocks:
            - text: ''
    - name: benefits-icons
      data:
        use_icon_component: true
        column_size: 10
        benefits:
          - title: Speed
            icon:
              name: stopwatch
              alt: stopwatch icon
            text: Developers can remediate vulnerabilities while they’re coding, which teaches secure code writing and reduces back and forth during security reviews.
          - title: Collaboration
            icon:
              name: collaboration
              alt: collaboration icon
            text: Encouraging a security mindset across your app dev team aligns goals with security and encourages employees to work with others outside their functional silo.
          - title: Efficiency
            icon:
              name: gitlab-cd
              alt: gitlab-cd icon
            text: DevSecOps saves time, money, and employee resources over every launch and iteration - all valuable assets to IT and security teams suffering from skills and budget shortages.
    - name: topics-cta
      data:
        title: Manage your toolchain before it manages you
        text: |
            Visible, secure, and effective toolchains are difficult to come by due to the increasing number of tools teams use, and it’s placing strain on everyone involved. This study dives into the challenges, potential solutions, and key recommendations to manage this evolving complexity.
        column_size: 10
        cta_one:
          text: Read the study
          link: /resources/whitepaper-forrester-manage-your-toolchain/
          data_ga_name: Read the study
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devops.png"
            href: "https://www.youtube.com/watch?v=4ROYvNfRZVU&feature=emb_logo"
            data_ga_name: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
            data_ga_location: resource cards
          - icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            event_type: "Webcast"
            header: Citizen-Centric Resiliency In Challenging Times
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_8.jpeg"
            href: https://page.gitlab.com/webcast-citizen-centric-resiliency.html
            data_ga_name: Citizen-Centric Resiliency In Challenging Times
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Report"
            header: "GitLab’s 2020 Global DevSecOps Survey"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_11.jpeg"
            href: /developer-survey/
            data_ga_name: "GitLab’s 2020 Global DevSecOps Survey"
            data_ga_location: resource cards
          - icon:
              name: report
              variant: marketing
              alt: Report Icon
            event_type: "Article"
            header: "Best Practices for a DoD DevSecOps Culture"
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_14.jpeg"
            href: https://page.gitlab.com/resources-article-RightPlatformDoD.html
            data_ga_name: "Best Practices for a DoD DevSecOps Culture"
            data_ga_location: resource cards
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "Why you need static and dynamic application security testing in your development workflows"
            text: |
                  Bolster your code quality with static and dynamic application security testing. Read here on why you need SAST and DAST for your projects.
            link_text: "Learn more"
            href: /blog/2019/08/12/developer-intro-sast-dast/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "static and dynamic application security testing"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "4 Ways developers can write secure code with GitLab"
            text: |
                GitLab Secure is not just for your security team – it’s for developers too. Learn four ways to write secure code with GitLab.
            link_text: "Learn more"
            href: /blog/2019/09/03/developers-write-secure-code-gitlab/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "4 Ways developers can write secure code with GitLab"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "5 Security testing principles every developer should know"
            text: |
                Developers are looking for guidance and standard practices as they take on more security testing responsibilities.
            link_text: "Learn more"
            href: /blog/2019/09/16/security-testing-principles-developer/
            image: /nuxt-images/blogimages/scm-ci-cr.png
            data_ga_name: "5 Security testing principles every developer should know"
            data_ga_location: resource cards

  schema_faq:
    - question: What is DevSecOps?
      answer: Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. It is time to shift left by embracing security earlier within the DevOps lifecycle.
    - question: What is application security?
      answer: Application security has been [defined by TechTarget](https://searchsoftwarequality.techtarget.com/definition/application-security) as the use of software, hardware, and procedural methods to protect applications from external threats. Modern approaches include shifting left to find and fix vulnerabilities earlier, while also shifting right to protect your applications and their infrastructure-as-code in production. Securing the Software Development Life Cycle (SDLC) itself is often a requirement as well.
