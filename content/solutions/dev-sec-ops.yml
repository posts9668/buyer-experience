---
  title: Integrating security into your DevOps Lifecycle
  description: GitLab application security testing for SAST, DAST, Dependency scanning, Container Scanning and more within the DevSecOps CI pipeline with vulnerability management and compliance.
  components:
    - name: 'solutions-hero'
      data:
        title: DevSecOps with GitLab
        subtitle:  Integrating security into your DevOps lifecycle is easy with GitLab. Security and compliance are built-in, out of the box, giving you the visibility and control necessary to protect the integrity of your software.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Join GitLab
          url: /free-trial/
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: devsecops with gitlab"
    - name: copy-media
      data:
        block:
          - header: The DevOps platform that simplifies DevSecOps
            text: |
              GitLab is known for industry-leading Source Code Management (SCM) and Continuous Integration (CI). Developers want to use GitLab. We make it easy for them to develop more secure and compliant software. The GitLab DevOps platform shifts both security and compliance earlier in the development process with consistent pipelines that automate scanning and policies. Uniting developers and security pros within one platform streamlines vulnerability management for both and improves collaboration.
            miscellaneous: |

              *   **Application security testing and remediation.** With every code commit, GitLab provides actionable vulnerability [findings to developers](https://docs.gitlab.com/ee/user/application_security/){data-ga-name="findings to developers" data-ga-location="body"} while helping security pros [manage remaining vulnerabilities](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center){data-ga-name="manage remaining vulnerabilites" data-ga-location="body"} through resolution.

              *   **Cloud Native Application Protection.** GitLab helps you monitor and protect your deployed containerized applications.

              *   **Policy Compliance and Auditability.** GitLab’s MR approvals, end-to-end transparency of who changed what, when, and where, along with a compliance dashboard and [common controls](https://docs.gitlab.com/ee/administration/compliance.html){data-ga-name="common controls" data-ga-location="body"} help you meet your [compliance needs](/solutions/compliance/){data-ga-name="compliance needs" data-ga-location="body"}.

              *   **SDLC Platform Security.** See how we secure the [GitLab software](/security/){data-ga-name="gitlab software" data-ga-location="body"}.
    - name: 'solutions-feature-list'
      data:
        title: 'The Gitlab Difference'
        subtitle: ''
        icon:
          name: gitlab
          alt: GitLab Icon
          variant: marketing
        features:
          - title: Simplicity
            description: |
              One platform, one price, with comprehensive application security.

              *   [Application Security Testing](https://docs.gitlab.com/ee/user/application_security/){data-ga-name="application security testing" data-ga-location="body"}
              *   [Vulnerability Management](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/){data-ga-name="vulnerability management" data-ga-location="body"}
              *   [Scan Deployed Images](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/){data-ga-name="deployed images" data-ga-location="body"}
            icon:
              name: doc
              alt: Doc Icon
              variant: marketing
              hex_color: '#380D74'
          - title: Control
            description: |
              Compliance framework for consistency, common controls, policy automation.

              *   [Common compliance controls](https://docs.gitlab.com/ee/administration/compliance.html){data-ga-name="compliance capabilites" data-ga-location="body"}
              *   [Security Policy Configuration](https://docs.gitlab.com/ee/user/application_security/configuration/){data-ga-name="security policy configuration" data-ga-location="body"}
              *   [Compliant pipelines](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration)
            icon:
              name: shift-left
              alt: Shift Left Icon
              variant: marketing
              hex_color: '#380D74'
          - title: Visibility
            description: |
              See who changed what, where, when, end-to-end.

              *   [Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html){data-ga-name="audit events" data-ga-location="body"}
              *   [Audit Reports](https://docs.gitlab.com/ee/administration/audit_reports.html){data-ga-name="audit reports" data-ga-location="body"}
              *   [Dependency List (BOM)](https://docs.gitlab.com/ee/user/application_security/dependency_list/#dependency-list){data-ga-name="dependency list" data-ga-location="body"}
            icon:
              name: shield-check
              alt: Shield Check Icon
              variant: marketing
              hex_color: '#380D74'

    - name: 'copy-media'
      data:
        block:
          - header: DevSecOps simplified
            text: |
              Continuous security testing capabilities
            video:
              video_url: https://www.youtube.com/embed/XnYstHObqlA?enablesjsapi=1
    - name: copy
      data:
        block:
          - header: Capabilities included within the GitLab Ultimate tier
            align_center: true
            hide_horizontal_rule: true
    - name: copy-media
      data:
        block:
          - header: Comprehensive Application Security Scanning for developers
            text: |
              Shift security left to empower developers to find and fix security flaws as they are created.
              *   Automatically include [application security testing](https://docs.gitlab.com/ee/user/application_security/){data-ga-name="appsec testing" data-ga-location="body"} in your CI pipelines - one tool, one cost, one user interface, one source of truth to unite dev and sec.

              *   Provide [actionable scan results](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/){data-ga-name="vulnerabilities action" data-ga-location="body"} to the developer to assess and resolve potential vulnerabilities at code commit, before code is merged - even for DAST.

              *   [Auto Remediation](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#resolve-a-vulnerability){data-ga-name="remediation" data-ga-location="body"} automatically creates a patch to resolve some vulnerabilities.

              *   Scanners include [SAST](https://docs.gitlab.com/ee/user/application_security/sast/){data-ga-name="static scanning" data-ga-location="body"} , [DAST](https://docs.gitlab.com/ee/user/application_security/dast/){data-ga-name="dynamic scanning" data-ga-location="body"}, [Dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/){data-ga-name="dependency scanning" data-ga-location="body"}, [License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/){data-ga-name="license compliance" data-ga-location="body"}, [Container scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/), [Cluster Image Scanning](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/[]), web API testing, [Infrastructure-as-code](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) (IaC) testing, [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/){data-ga-name="secret detection" data-ga-location="body"}
            image:
              image_url: /nuxt-images/solutions/sast-screen-shot.png
              alt: ""
            inverted: true
          - header: Vulnerability Management for security pros
            text: |
              Assess and triage vulnerabilities that remain after code changes are merged.
              *   Security pros can [manage vulnerabilities](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/){data-ga-name="manage vulnerabilities" data-ga-location="body"} across projects and groups to evaluate and [triage vulnerabilities](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#vulnerability-report-actions){data-ga-name="report fields" data-ga-location="body"}.

              *   Dynamically test running web applications [on demand](https://docs.gitlab.com/ee/user/application_security/dast/#on-demand-scans){data-ga-name="dast on demand" data-ga-location="body"} for known runtime vulnerabilities.

              *   Show all dependencies used in a project via a [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_list/){data-ga-name="dependency list" data-ga-location="body"} (also called a Bill of Materials).

              *   [Export](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#export-vulnerability-details){data-ga-name="export vulnerabilities" data-ga-location="body"} findings, [import](https://docs.gitlab.com/ee/development/integrations/secure.html){data-ga-name="import vulnerabilities" data-ga-location="body"} findings from their party scanners and bug bounties. [Filter by scanner vendor](https://docs.gitlab.com/ee/development/integrations/secure.html#report-fields){data-ga-name="report fields" data-ga-location="body"}
            image:
              image_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/img/project_level_vulnerability_report_v14_5.png
              alt: ""
            inverted: true
          - header: Security and Compliance Governance
            text: |
              Automate security and compliance policies across your software development lifecycle.
              *   [Compliant pipelines](https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation){data-ga-name="compliant pipelines" data-ga-location="body"} for consistent use of security policies. Security configuration via check-boxes and granular controls - no need to code pipelines.

              *   [Security dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center) at the project, group, and instance level, along with a personalized view of specific projects.

              *   [Policy management](https://docs.gitlab.com/ee/administration/compliance.html#policy-management){data-ga-name="policy management" data-ga-location="body"} for MR approvals, separation of duties and other common controls, including a [Compliance Report](https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html#view-the-compliance-report-for-a-group).
            image:
              image_url: https://docs.gitlab.com/ee/user/application_security/policies/img/policies_list_v14_3.png
              alt: ""
            inverted: true
          - header: Cloud native security
            text: |
              *   [Container scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/#container-scanning){data-ga-name="container scanning" data-ga-location="body"}, [cluster image scanning](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/){data-ga-name="cluster image scanning" data-ga-location="body"}, [Infrastructure-as-code (IaC) scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/){data-ga-name="compliant pipelines" data-ga-location="body"} , [web API fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/){data-ga-name="api fuzzing" data-ga-location="body"}. All scan results are provided to the developer within their CI pipeline alongside more traditional scan results - no do-it-yourself integration is required.
            image:
              image_url: https://docs.gitlab.com/ee/user/application_security/container_scanning/img/container_scanning_v13_2.png
              alt: ""
            inverted: true
    - name: copy-media
      data:
        block:
          - header: Additional Capabilities within GitLab Ultimate
            miscellaneous: |
              *   **Fuzz Testing** - Fuzz testing acquisitions have been integrated alongside other scanners in the merge request pipeline. Apply this powerful technology to automatically test for unknown security flaws with [coverage-guided](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/){data-ga-name="coverage guided" data-ga-location="body"} fuzzing and [API fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/){data-ga-name="API fuzzing" data-ga-location="body"}
              *   **Offline Environments** - self-managed customers can run most of the GitLab security scanners [when not connected to the internet](https://docs.gitlab.com/ee/user/application_security/offline_deployments/#offline-environments){data-ga-name="offline environments" data-ga-location="body"}
              *   **Mobile app testing** - [Test mobile applications](/blog/2020/12/16/mobile-static-application-security-testing-for-android/){data-ga-name="SAST" data-ga-location="body"} within your CI pipeline including Kotlin, Swift, Objective-C, and Java.
    - name: copy
      data:
        block:
          - header: Capabilities included within all GitLab tiers
            align_center: true
            hide_horizontal_rule: true
    - name: copy-media
      data:
        block:
          - header: Basic Application Security
            text: |
              *   [SAST](https://docs.gitlab.com/ee/user/application_security/sast/){data-ga-name="sast" data-ga-location="body"} and [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/){data-ga-name="secret detection" data-ga-location="body"} are automatically includeed in your CI pipelines - with no integration required.

              *   Provide basic scan results to the developer at code commit, before code is merged. Results may be downloaded for analysis. Note that use of interactive findings in the [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/){data-ga-name="static testing" data-ga-location="body"} requires the GitLab Ultimate tier.

            image:
              image_url: https://docs.gitlab.com/ee/user/application_security/img/security_widget_v13_7.png
              alt: ""
          - header: Why integration matters for DevSecOps
            miscellaneous: |
              *   **Every piece of code** is tested upon commit for security threats, without incremental cost.
              *   **The developer** can remediate now, while they are still working in that code, or create an issue with one click.
              *   **The security pro** can see and manage unresolved vulnerabilities captured as a by-product of software development.
              *   **Single source of truth** can focus collaboration on remediation, eliminating translation and finger pointing.
              *   **A single tool** reduces cost to buy, integrate and maintain point solutions throughout the DevOps pipeline.
    - name: 'solutions-feature-list'
      data:
        title: Meeting your business objectives
        subtitle: We welcome your feedback and contribution to our [vision and roadmap](/direction/secure/){data-ga-name="vision and roadmap" data-ga-location="body"}
        icon:
          name: gitlab
          alt: GitLab Icon
          variant: marketing
        features:
          - title: Shift security and compliance left
            description: |
              Empower developers to find and fix flaws.
              *   Keep it simple. No need to integrate and maintain disparate tools. One tool, [one price](/pricing/){data-ga-name="pricing" data-ga-location="body"}
              *   Findings from [all scanners](https://docs.gitlab.com/ee/user/application_security/){data-ga-name="appsec scanners" data-ga-location="body"} within the developer's pipeline.
            icon:
              name: doc
              alt: Doc Icon
              variant: marketing
              hex_color: '#380D74'
          - title: Consistently compliant pipelines
            description: |
              Easily ensure pipelines consistently meet policy requirements
              *   Choose your [compliance framework](https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation){data-ga-name="compliance framework" data-ga-location="body"} and automatically apply it to every pipeline.
              *   [Security configuration](https://docs.gitlab.com/ee/user/application_security/configuration/#security-configuration){data-ga-name="security config" data-ga-location="body"} for security pros (without coding yml)
            icon:
              name: shift-left
              alt: Shift Left Icon
              variant: marketing
              hex_color: '#380D74'
          - title: Software Supply Chain Security
            description: |
              Protect your applications and their surrounding infrastructure.
              *   Manage [security policies](https://docs.gitlab.com/ee/administration/compliance.html#policy-management){data-ga-name="security policies" data-ga-location="body"} across the software development lifecyle.
              *   [Auditability](https://docs.gitlab.com/ee/administration/compliance.html#audit-management){data-ga-name="vision and roadmap" data-ga-location="body"} and traceability to see who changed what, where, from planning through production.
            icon:
              name: shield-check
              alt: Shield Check Icon
              variant: marketing
              hex_color: '#380D74'
    - name: 'solutions-resources'
      data:
        title: 'Resources'
        list: |
          * Learn how to [add Security to your CICD Pipeline](https://youtu.be/Fd5DhebtScg)
          * Efficiently manage vulnerabilities and risk using the [GitLab Security Dashboards](https://youtu.be/p3qt2z1rQk8)
          * Manage your [Application Dependencies](https://youtu.be/scNS4UuPvLI)
          * Use GitLab Application Security Capabilities [with Jenkins](https://youtu.be/8VoxulxxM4Y)
          * See how we [compare](/devops-tools/){data-ga-name="see how we compare" data-ga-location="body"} against other Security tools
