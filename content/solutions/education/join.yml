---
  title: Join the GitLab for Education Program
  description: GitLab for Education bringing DevOps to a classroom near you. Apply today to get started on your DevOps journey!
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Education
        subtitle: Bringing DevOps to your Campus
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/education/join/
          text: Join the GitLab for Education Program
          data_ga_name: join education program
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "Image: gitlab for education"
          rounded: true
    - name: tabs-menu
      data:
        column_size: 8
        menus:
          - id_tag: requirements
            text: Requirements
            data_ga_name: requirements
            data_ga_location: header
          - id_tag: application
            text: Application
            data_ga_name: application
            data_ga_location: header
          - id_tag: renewal
            text: Renewal
            data_ga_name: renewal
            data_ga_location: header
          - id_tag: frequently-asked-questions
            text: Frequently Asked Questions
            data_ga_name: frequently asked questions
            data_ga_location: header
    - name: copy-media
      data:
        block:
          - header: Requirements
            aos_animation: fade-up
            aos_duration: 500
            hide_horizontal_rule: true
            metadata:
              id_tag: requirements
            miscellaneous: |
              ##### In order to be accepted into the GitLab for Education Program, each educational institution must meet the following requirements.

              * **Accredited:** the Educational Institution must be accredited by a local, state, provincial, federal, or national authorized agency. [See more details](/handbook/marketing/community-relations/education-program/#gitlab-for-education-program-requirements){data-ga-name="accredited" data-ga-location="body"}.
              * **Primary purpose teaching:** the Educational Institution must have the primary purpose of teaching its enrolled students.
              * **Degree granting:** the Educational Institution actively grant degrees such as a high school diploma, associate's degree, bachelor's degree, master's degree, or doctor of philosphy.
              * **Not-for-profit:** the Educational Institution must be not-for-profit. For-profit entities are not eligible.

              ##### GitLab for Education Licenses can only be used for

              * **Instructional Use:** activites directly related to learning, training, or development of students including academic instruction that are part of the instructional functions of the Educational Institution or
              * **Non-commerical academic research:** conducting not-for-profit research projects that do not produce results, works, services, or data for commerical use by anyone to generate revenue. Research conducted at the request of and for the benefit of a third party is not authorized under the GitLab for Education license.
              * **It is not authorized to run, administer, or operate an insitution with the GitLab for Education license.** GitLab offers academic discounts and special pricing for campus-wide use. [See more details](/solutions/education/campus/){data-ga-name= "campus pricing" data-ga-location="body"}.

              * **Note:** At this time, institutions enrolling students under the age of 13 are not eligible for the GitLab SaaS. These institutions may obtain a GitLab self-managed license.

              ##### Applicants must

              * **Faculty or Staff:** only faculty or staff employed full-time at an Educational Institution can apply. We are not able to issue licenses to students directly.
              * **Email domain:** applicants must apply with the email domain of their representative institution. Personal email domains will not be accepted.

              ##### Country of Origin

              * GitLab, Inc. does not issue licenses to educational institutions located in China. For more information on obtaining an educational license in China, please [contact JiHu](mailto:ychen@gitlab.cn). [Read more about JiHu..](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/){data-ga-name="more about JiHu" data-ga-location="body"}

              #### GitLab for Education Agreement

              * Upon acceptance, all program members are subject to the [GitLab for Education Program Agreement](/handbook/legal/education-agreement/){data-ga-name="education agreement" data-ga-location="body"}.

              #### Program Benefits

              * Unlimited seats per license of our top-tier functionality (SaaS or self-managed). The number of seats is the number of different users that will use this license during the next year.
              * The number of seats and type of license (Saas or self-managed) can be changed at the time of renewal or upon request.
              * GitLab support is not included with the educational license.
              * 50,000 CI runner minutes are included with the subscription. ([Additional minutes must be purchased](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes){data-ga-name="additional minutes" data-ga-location="body"}).
    - name: copy-form
      data:
        form:
          external_form:
            url: https://offers.sheerid.com/gitlab/university/teacher/
            width: 800
            height: 1300
        header: Application
        metadata:
          id_tag: application
        form_header: Application Form
        datalayer: sales

        content: |
          ## Application process
          * Fill out the application form on the right. Please provide the most accurate and complete information as possible.
          * Gitlab uses SheerID, a trusted partner, to verify that you are a current teacher, faculty, or staff member at a qualified educational institution.

          ## What to expect
          After you complete the application form, if you are verifed, you will recieve a verification email with instructions to obtain your license. Please follow the instructions carefully.

          ## Help and support
          If you have any issues obtaining your license in the Customer Portal please open a support ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) and Select “Licensing and Renewal Problems.”
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - header: Renewal
            metadata:
              id_tag: renewal
            subtitle: Email a renewal request
            column_size: 10
            text: |
              You will need to email us annually to renew your membership.

              Please begin the renewal process **at least one month in advance** to ensure sufficient processing time. You will receive email reminders to do so and can renew as early as three months in advance of your membership's expiration.

              To renew, please send an email to [education@gitlab.com](mailto:education@gitlab.com) with the following information (copy and paste):

              > #### Subject: Renewal Request | Date of Expiration (MM/YYYY)
              >
              > To help us find your account:
              > *  Name of your organization or project
              >
              > *  Email associated with this account
              >
              > To help us make sure you still qualify:
              > *  Please verify the use case for the education license. Professional IT and infrastructure operations do not fall within the stated terms of the Education Program.
              >
              > To help us plan for next year:
              > *  Number of seats you are renewing.
              >
              > *  Any change of ownership to the account.
              >
              > * Please verify the license type to be renewed.
              >
              > * Please attach a PDF version of last year's quote if avaiable.
              >
              > *  If the ownership needs to change, please send the new account holder's name, email address, and contact mailing address.

              Our program requirements may change from time to time, and we'll need to make sure that you continue to meet them year after year. If you do not qualify upon renewal, we'll work with you to make any needed transition as smooth as possible.

              If you do not renew your membership for any reason, [your account will be downgraded](/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired){data-ga-name="account downgraded" data-ga-location="body" target="_blank"}.

              ### What to expect
              If your renewal request is accepted, we'll ask you to sign a zero dollar renewal quote. After that, here's what to expect for each type of renewal:

              * **Saas renewals:** No further action is necessary after your renewal is fully processed. Your account will automatically renew.
              * **Self-managed renewals:** You'll need to obtain your license from the ([GitLab Customer Portal](https://customers.gitlab.com/){data-ga-name="customer portal" data-ga-location="body" targed="_blank"}) and upload it to your instance. If you need help at any point, [we are here to support you](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs){data-ga-name="support" data-ga-location="body" target="_blank"}.

              > #### Service Level Agreement:
              > It may take up to 15 business days to recieve a response. Renewal applications are processed in the order in which they are  recieved.
              >
              > Applications will **not be processed during U.S. Holidays and responses may be delayed during those periods**.
              >
              > Furthermore, a few extra steps are required before we transition to a more automated application process. We appreciate your patience. To help us prioritize, please let us know if your request is urgent by including "Urgent: [your request]" in the subject line of any email to education@gitlab.com.

    - name: faq
      data:
        metadata:
          id_tag: frequently-asked-questions
        aos_animation: fade-up
        aos_duration: 500
        header: Frequently Asked Questions
        groups:
          -
            questions:
              - question: Does research qualify for the Education license?
                answer: |
                  Yes, research qualifies if the educational institution qualifies and the research is non-commercial academic research. Please see our program requirements for more details.
              - question: Can we run multiple self-managed instances with the same license key?
                answer: |
                  Yes. It is possible to activate multiple self-managed instances with the same license key.
              - question: Why doesn't GitLab provide licenses for free directly to students?
                answer: Our GitLab Education Program offering is intended to be issued directly to the institution (enterprise-level) rather than individuals. We offer an unlimited number of seats and top-tier licenses so that all students at an institution can access the best GitLab has to offer. We understand that individual students may wish to have a GitLab license but at this time, we do not have the logistics in place to grant individuals licenses. We encourage all students to find a faculty or staff sponsor and apply to the program. Students should also check out our free subscription tier for GitLab.com or a free download of our Free self-managed offering. You can also apply for a 30-day trial if you’d like to try out some more advanced features.
              - question: How can I manage the visibility of our projects?
                answer: |
                  If you're a member of the parent group in GitLab, you'll automatically have access to all descendants. GitLab doesn't support subgroups that are more restrictive than their parent groups. However, being a part of a subgroup does not grant you access to the parent group.
                  \
                  The best way to do this is to make everyone a member of their respective subgroup having only admins in the top level group.
              - question: Can this license be used in the IT department for running IT services?
                answer: |
                  No, IT professional use or any administrative use for running the institution itself does not qualify. The GitLab Education license can only use for teaching or research purposes. Please contact our sales team if you are interested in using GitLab for IT professional use.
              - question: Can students use our GitLab instance after they graduate?
                answer: |
                  GitLab Education licenses are issued to the educational institution directly. Therefore, students will need to purchase their own license if they are no longer provided access by their institution.
              - question: Are modifications to the end user license agreement allowed?
                answer: |
                  At this time, we are not able to accommodate modifications to our user agreement. Please email education@gitlab.com. if you have any additional questions.
              - question: Is it possible to authenticate users via LDAP over SSL?
                answer: |
                  It is possible only on our self-managed version. The server doesn't strictly need a static IP, as a DNS name can be used for the LDAP server.
              - question: Is it possible to increase the number of seats in the future?
                answer: |
                  If you wish to increase the number of seats on your existing license, please send an email to education@gitlab.com, and we’ll prepare an add-on quote for additional seats.
              - question: Who gets counted in the subscription?
                answer: |
                  Please see our licensing and subscription FAQ for the detailed explanation.
              - question: How do I get support?
                answer: |
                   Please see the [Support for Community Programs](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs) docs sections for a detailed description of where to find support. Please note that it is no longer an option to purchase support separately for GitLab for Education licenses. Instead, qualifying institutions have the option to purchase the [GitLab for Campuses subscription](/solutions/education/campus/).
