---
  title: All Remote
  description: "GitLab is one of the world's largest all-remote companies"
  call_to_action:
    title:  A world leader in remote work
    shadow: true
    image_border: true
    subtitle: Learn how we’ve scaled as an asynchronous, no-office company that’s consistently recognized as a great place to work.
    free_trial_button:
      text: Get the Remote Playbook
      url: https://learn.gitlab.com/allremote/remote-playbook
      data_ga_name: Get the remote playbook
      data_ga_location: header
    contact_sales_button:
      text: Discover MECC
      url: /handbook/mecc/
      data_ga_name: MECC — GitLab's Management Philosophy
      data_ga_location: header
    image:
      image_url: /nuxt-images/all-remote/remote-playbook-cover-2022.jpeg
      alt: 2022 Remote Playbook Link
      image_href: https://learn.gitlab.com/allremote/remote-playbook
  feature_list:
    inner_icon: true
    title: "The complete guide to remote work"
    subtitle: "All-remote since inception, GitLab applies the DevOps Platform approach to innovate how people collaborate, communicate, and deliver results in 65+ countries. Our knowledge and best practices are open to the public."
    icon:
      name: "remote-work"
      alt: "Remote Work Icon"
      variant: marketing
    features:
      - title: "Remote-work essentials"
        icon:
          name: "remote-earth"
          alt: "Remote Earth Icon"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: "Compare: All-Remote vs. Hybrid-Remote"
            url: /company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/
            data_ga_location: 'body'
            data_ga_name: "Compare: All-Remote vs. Hybrid-Remote"
          - title: "How to transition a company to remote operation"
            url: /company/culture/all-remote/transition/
            data_ga_location: 'body'
            data_ga_name: "company transition to remote operation"
          - title: "What not to do when implementing remote work"
            url: /company/culture/all-remote/what-not-to-do/
            data_ga_location: 'body'
            data_ga_name: "What not to do when implementing remote work"
      - title: "Designing a remote team"
        icon:
          name: "remote-world"
          alt: "Remote World Icon"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: About the Head of Remote role
            url: /company/culture/all-remote/head-of-remote/
          - title: The 10 models of remote teams
            url: /company/culture/all-remote/stages/
          - title: The importance of documentation for remote teams
            url: /company/culture/all-remote/handbook-first-documentation/
      - title: Remote hiring & onboarding
        icon:
          name: "remote-user-laptop"
          alt: "Remote User Laptop Icon"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: How to hire for a remote team
            url: /company/culture/all-remote/hiring/
          - title: The guide to remote onboarding
            url: /company/culture/all-remote/onboarding/
          - title: How to evaluate a remote job opportunity
            url: /company/culture/all-remote/evaluate/
      - title: Collaboration & async
        icon:
          name: "remote-idea-collaboration"
          alt: "Remote Idea Collaboration Icon"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: How to work asynchronously
            url: /company/culture/all-remote/asynchronous/
          - title: How to run effective meetings remotely
            url: /company/culture/all-remote/meetings/
          - title: Remote collaboration and whiteboarding
            url: /company/culture/all-remote/collaboration-and-whiteboarding/
      - title: Scaling remote culture
        icon:
          name: "collaboration-alt"
          alt: "Collaboration Icon"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: How to build culture on remote teams
            url: /company/culture/all-remote/building-culture/
          - title: The importance of informal communication
            url: /company/culture/all-remote/informal-communication/
          - title: Building an inclusive remote culture
            url: /company/culture/inclusion/building-diversity-and-inclusion/
      - title: Learn more
        icon:
          name: "remote-handbook"
          alt: "Remote Handbook Icon"
          variant: marketing
        description: "For in-depth resources on everything you need to know about remote work, see GitLab’s full Guide to Remote."
        remove_bullet_points: true
        subfeatures:
          - title: Read GitLab’s full Guide to Remote
            url: /company/culture/all-remote/guide/
  highlights_1:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-1.png"
          alt: ""
        url: "https://store.hbr.org/product/gitlab-and-the-future-of-all-remote-work-a/620066"
        impact: "GitLab and the Future of All-Remote Work"
        logo: "/nuxt-images/all-remote/harvard_business_review_logo.svg"
        logo_alt: Harvard Business Review Logo
        stats:
          - qualifier: "Value creation of the all-remote model"
          - qualifier: "Organizational processes at scale"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-2.png"
          alt: ""
        logo: "/nuxt-images/all-remote/remote-logo-white.svg"
        logo_alt: Remote Logo
        url: "/customers/remote/"
        name: "Remote"
        impact: " meets 100% of deadlines with GitLab."
        stats:
          - qualifier: "Single source of truth"
          - qualifier: "Zero context switching"
  highlights_2:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-3.png"
          alt: ""
        logo: "/nuxt-images/all-remote/insead.svg"
        logo_alt: Insead Logo
        url: "https://publishing.insead.edu/case/gitlab"
        impact: "GitLab: Can “All Remote” Scale?"
        stats:
          - qualifier: "Examples of asynchronous work"
          - qualifier: "Discussion of organizational design"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-4.png"
          alt: ""
        logo: "/nuxt-images/all-remote/hotjar_logo.svg"
        logo_alt: Hotjar Logo
        url: "/customers/hotjar/"
        impact: "Fully-remote team Hotjar deploys 50% faster with GitLab."
        stats:
          - qualifier: "End-to-end visibility"
          - qualifier: "All-remote operations"
  events:
    icon:
      name: ribbon-check
      alt: Ribbon Check Icon
      variant: marketing
    header: Earn a Remote Work Certification
    cards_per_row: 2
    description: Become an expert in essential skills for success in a distributed workplace.
    events:
      - heading: How to Manage a Remote Team
        date: February 23, 2021
        event_type: Course
        destination_url: 'https://www.coursera.org/learn/remote-team-management'
      - heading: Remote Foundations Training
        date: February 23, 2021
        event_type: Training
        destination_url: https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge
  blog_cards:
    header: More Remote Work Knowledge on the GitLab Blog
    more:
      title:  Deep dive into remote best practices for DevOps, engineering, team culture, inclusion, and much more.
      button_url: /blog/categories/culture/
      button_text: Read the latest
      data_ga_name: read the latest
      data_ga_location: body
    cards:
      - category: Scale
        title: 5 Ways to scale remote work on your team
        portrait_url: https://gitlab.com/uploads/-/system/user/avatar/3461712/avatar.png?width=400
        blog_url: /blog/2021/08/09/five-ways-to-scale-remote-work/
        author_name: Betsy Bula
      - category: Management
        title: Tips for managing remote working engineering teams
        blog_url: /blog/2021/01/29/tips-for-managing-engineering-teams-remotely/
        author_name: Sara Kassabian
      - category: Design
        title: How we carry out remote work UX and Design
        image_url: "/nuxt-images/blogimages/laptop.png"
        image_alt: ""
        blog_url: /blog/2020/03/27/designing-in-an-all-remote-company/
        author_name: Christy Lenneville
        portrait_url: https://gitlab.com/uploads/-/system/user/avatar/3473743/avatar.png
  video_carousel:
    videos:
      - title: "Why Work Remotely?"
        photourl: /nuxt-images/all-remote/2.jpg
        video_link: https://www.youtube-nocookie.com/embed/GKMUs7WXm-E
        carousel_identifier:
          - ''

      - title: "The Mechanics of All Remote"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-webcast-the-mechanics.jpg
        video_link: https://www.youtube-nocookie.com/embed/9xvVMglCm6I
        carousel_identifier:
          - ''

      - title: "Preventing Burnout and Achieving Balance"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-preventing-burnout.jpg
        video_link: https://www.youtube-nocookie.com/embed/Q9yjo6IOqX4
        carousel_identifier:
          - ''

      - title: "Rethinking Remote Management"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-rethink-remote-management.jpg
        video_link: https://www.youtube-nocookie.com/embed/dsnSb-lpZSI
        carousel_identifier:
          - ''

      - title: "Tips for a productive all-remote workforce"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-tips-for-a-productive-workforce.jpg
        video_link: https://www.youtube-nocookie.com/embed/CsLswGz6J5s
        carousel_identifier:
          - ''

      - title: "Sid and Dominic discuss all remote work"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-ceo-dominic-monn.jpg
        video_link: https://www.youtube-nocookie.com/embed/EeUhxQn_ct4
        carousel_identifier:
          - ''

      - title: "Managing Exceptional Teams"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-managing-exceptional-teams.jpg
        video_link: https://www.youtube-nocookie.com/embed/6VVAsMEKqFU
        carousel_identifier:
          - ''

      - title: "Evolving a Fully Remote Company Culture"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-remote-fireside-chat.jpg
        video_link: https://www.youtube-nocookie.com/embed/hVUzXmjKQJ0
        carousel_identifier:
          - ''

      - title: "Using video for effective collaboration"
        photourl: /nuxt-images/all-remote/vid-thumb-universal-using-video-collaboration.jpg
        video_link: https://www.youtube-nocookie.com/embed/6mZqzK_40FE
        carousel_identifier:
          - ''
  resources:
    title: Future of Work Trends & Research
    hide_horizontal_rule: true
    block:
      - text: Gain deeper insight into the most pressing topics affecting remote and hybrid workplaces.
        resources:
          report:
            header: Reports
            links:
              - text: 'The Remote Work Report'
                link: /remote-work-report/
                data_ga_name: 'The remote work report'
                data_ga_location: body
              - text: 'Work-from-Home Field Guide'
                link: /work-from-home-field-guide/
                data_ga_name: 'work from home guide'
                data_ga_location: body
              - text: 'Out of the Office'
                link: /out-of-the-office/
                data_ga_name: 'out of the office'
                data_ga_location: body
